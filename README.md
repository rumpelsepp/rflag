# rflag (rumpelsepp's flag)

[![GoDoc](https://godoc.org/git.sr.ht/~rumpelsepp/rflag?status.svg)](https://godoc.org/git.sr.ht/~rumpelsepp/rflag)

Pflag is a replacement for Go's flag package, implementing
POSIX/GNU-style --flags.

pflag is compatible with the [GNU extensions to the POSIX recommendations
for command-line options][1]. For a more precise description, see the
"Command-line flag syntax" section below.

[1]: http://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html

pflag is available under the same style of BSD license as the Go language,
which can be found in the LICENSE file.

